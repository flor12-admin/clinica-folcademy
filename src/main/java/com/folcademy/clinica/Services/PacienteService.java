package com.folcademy.clinica.Services;




import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.OnlyLettersException;
import com.folcademy.clinica.Exceptions.OnlyNumbersException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Entities.Paciente;

import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PacienteService /*implements IPacienteService*/ {
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;
    public int acumulador = 0;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }


    public List<Paciente> findAllPacientes() {
        if (pacienteRepository.findAll().isEmpty())
            throw new NotFoundException("No hay pacientes registrados");
        return (List<Paciente>) pacienteRepository.findAll();

    }
    public List<Paciente> findPacienteById(Integer id) {
        List<Paciente> lista = new ArrayList<>();

        if (!pacienteRepository.existsById(id))
            throw new NotFoundException("Paciente no encontrado");

        Paciente paciente = pacienteRepository.findById(id).get();
        lista.add(paciente);
        return lista;
    }

    static boolean esSoloLetras(String cadena) {
        for (int i = 0; i < cadena.length(); i++) {
            char caracter = cadena.toUpperCase().charAt(i);
            int valorASCII = (int) caracter;
            if (valorASCII != 165 && (valorASCII < 65 || valorASCII > 90))
                return false;
        }
        return true;
    }

    public static boolean esSoloNumeros(String cadena) {
        return cadena.matches("[0-9]+");
    }


    public PacienteDto save (PacienteDto entity){
        entity.setIdpaciente(null);


        if(entity.getTelefono().length()>10){
            throw new ValidationException("Supera la cantidad de numeros");
        }
        if(entity.getDni().equals(""))
        {
            throw new ValidationException("Ingrese un dni");
        }

        String cadenadni = entity.getDni();;
        if(esSoloNumeros(cadenadni) == false || cadenadni.isEmpty())
            throw new OnlyNumbersException("El Dni contiene caracteres que NO son numeros");


        String cadena = entity.getNombre();
        String cadena2 = entity.getApellido();
        if (esSoloLetras(cadena) == false || cadena.isEmpty())
            throw new OnlyLettersException("El nombre contiene caracteres que NO son letras");
        if (esSoloLetras(cadena2) == false || cadena.isEmpty())
            throw new OnlyLettersException("El apellido contiene caracteres que NO son letras");



        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(entity)));

    }

    public PacienteEnteroDto editar (Integer idpaciente, PacienteEnteroDto dto){

        if(dto.getTelefono().length()>10){
            throw new ValidationException("Supera la cantidad de numeros");
        }


        /*if (!pacienteRepository.existsById(idpaciente))
            return null;*/
        dto.setIdpaciente(idpaciente);
        //dto -> entidad;
        //guardar;
        //entidad -> dto;
        return
                pacienteMapper.entityToEnteroDto(
                        pacienteRepository.save(
                                pacienteMapper.enteroDtoToEntity(dto)
                        )
                );
    }

    public boolean eliminar(Integer idpaciente) {


        if (!pacienteRepository.existsById(idpaciente))
            throw new NotFoundException("Paciente no encontrado");

        pacienteRepository.deleteById(idpaciente);
        return true;
    }
}


