package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.*;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TurnoService /*implements ITurnoService*/ {
    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;
    public int acumulador = 0;

    public TurnoService(TurnoRepository turnoRepository, TurnoMapper turnoMapper) {
        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
    }


    public List<Turno> findAllTurnos() {
        if (turnoRepository.findAll().isEmpty())
            throw new NotFoundException("No hay turnos registrados");
        return (List<Turno>) turnoRepository.findAll();
    }

    public List<Turno> findTurnoById(Integer id) {
        List<Turno> lista = new ArrayList<>();

        if (!turnoRepository.existsById(id))
            throw new NotFoundException("Turno no encontrado");

        Turno turno = turnoRepository.findById(id).get();
        lista.add(turno);
        return lista;
    }

    static boolean esSoloLetras(String cadena) {
        for (int i = 0; i < cadena.length(); i++) {
            char caracter = cadena.toUpperCase().charAt(i);
            int valorASCII = (int) caracter;
            if (valorASCII != 165 && (valorASCII < 65 || valorASCII > 90))
                return false;
        }
        return true;
    }

    public static boolean esSoloNumeros(String cadena) {
        return cadena.matches("[0-9]+");
    }


    public TurnoDto save (TurnoDto entity){

        entity.setIdturno(null);

        if (entity.getIdmedico()==0)
            throw new ValidationException("El id de médico debe ser mayor a 0");

        return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(entity)));

    }

    public boolean eliminar(Integer idturno) {
        if (!turnoRepository.existsById(idturno))
            throw new NotFoundException("Turno no encontrado");

        turnoRepository.deleteById(idturno);
        return true;
    }

    public TurnoEnteroDto editar (Integer idturno, TurnoEnteroDto dto){

        if (!turnoRepository.existsById(idturno))
            throw new NotFoundException("No existe el turno");

        dto.setIdturno(idturno);

        return
                turnoMapper.entityToEnteroDto(turnoRepository.<Turno>save(turnoMapper.enteroDtoToEntity(dto)));
    }

}