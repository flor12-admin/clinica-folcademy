package com.folcademy.clinica.Services;


import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.OnlyLettersException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;

import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Services.Interfaces.IMedicoService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class MedicoService /*implements IMedicoService*/ {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper){
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;

    }

    public List<MedicoDto> findAllMedico() {
        if (medicoRepository.findAll().isEmpty())
            throw new NotFoundException("No hay medicos registrados");
        return medicoRepository.findAll().stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
    }


    public List<MedicoDto> findMedicoById(Integer id) {
        List<MedicoDto> lista = new ArrayList<>();

        if (!medicoRepository.existsById(id))
            throw new NotFoundException("Medico no encontrado");

        MedicoDto medico = medicoRepository.findById(id).get();
        lista.add(medico);
        return lista;
    }

    public MedicoDto listarUno(Integer idmedico) {

        return medicoRepository.findById(idmedico).map(medicoMapper::entityToDto).orElse(null);
    }



    static boolean esSoloLetras(String cadena) {
        for (int i = 0; i < cadena.length(); i++) {
            char caracter = cadena.toUpperCase().charAt(i);
            int valorASCII = (int) caracter;
            if (valorASCII != 165 && (valorASCII < 65 || valorASCII > 90))
                return false;
        }
        return true;
    }


    public static boolean esSoloNumeros(String cadena) {
        return cadena.matches("[0-9]+");
    }


    public MedicoDto save (MedicoDto entity){
        entity.setIdmedico(null);

        if(entity.getConsulta()<=0){
            throw new BadRequestException("La consulta no puede ser menor a 0");
        }

        String cadena = entity.getNombre();
        String cadena2 = entity.getProfesion();
        String cadena3 = entity.getApellido();

        if (esSoloLetras(cadena) == false || cadena.isEmpty())
            throw new OnlyLettersException("La cadena contiene caracteres que NO son letras");

        if (esSoloLetras(cadena2) == false || cadena2.isEmpty())
            throw new OnlyLettersException("La cadena contiene caracteres que NO son letras");

        if (esSoloLetras(cadena3) == false || cadena3.isEmpty())
            throw new OnlyLettersException("La cadena contiene caracteres que NO son letras");

        return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(entity)));

    }

    public MedicoEnteroDto editar (Integer idmedico,MedicoEnteroDto dto){
        if (!medicoRepository.existsById(idmedico))
            //return null;
            throw new NotFoundException("No existe el medico");
        dto.setIdmedico(idmedico);
        //dto -> entidad;
        //guardar;
        //entidad -> dto;
        return
                medicoMapper.entityToEnteroDto(
                        medicoRepository.save(
                                medicoMapper.enteroDtoToEntity(dto)
                        )
                );
    }

    public boolean eliminar(Integer idmedico) {

        if (!medicoRepository.existsById(idmedico))
            throw new NotFoundException("Medico no encontrado");

        medicoRepository.deleteById(idmedico);
        return true;
    }
}
