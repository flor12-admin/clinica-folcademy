package com.folcademy.clinica.Controllers;


import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;

import com.folcademy.clinica.Model.Entities.Turno;

import com.folcademy.clinica.Model.Mappers.TurnoMapper;

import com.folcademy.clinica.Services.TurnoService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/turno")
public class TurnoController {

    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

    @PreAuthorize("hasAuthority('get_turno')")
    @GetMapping(value = "")
    public ResponseEntity<List<Turno>> findAll() {
        return ResponseEntity
                .ok()
                .body(
                        turnoService.findAllTurnos())
                ;
    }

    @PreAuthorize("hasAuthority('get_turno')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<List<Turno>> findAll(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok()
                .body(
                        turnoService.findTurnoById(id))
                ;
    }

    @PreAuthorize("hasAuthority('post_turno')")
    @PostMapping("")
    public ResponseEntity<TurnoDto> save(@RequestBody @Validated TurnoDto dto) {
        TurnoMapper turnoMapper = new TurnoMapper();
        return ResponseEntity.ok(turnoService.save(dto));
    }

    @PreAuthorize("hasAuthority('put_turno')")
    @PutMapping("/{id}")
    public ResponseEntity<TurnoEnteroDto> editar(@PathVariable(name = "id") int id,
                                                 @RequestBody TurnoEnteroDto dto) {
        return ResponseEntity.ok().body(turnoService.editar(id,dto));
    }

    @PreAuthorize("hasAuthority('delete_turno')")
    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "id") int id) {
        return ResponseEntity.ok(turnoService.eliminar(id));
    }
}
