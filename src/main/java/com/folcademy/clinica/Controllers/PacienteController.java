package com.folcademy.clinica.Controllers;


import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paciente")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }


    @PreAuthorize("hasAuthority('get_paciente')")
    @GetMapping(value = "")
    public ResponseEntity<List<Paciente>> findAll() {
        return ResponseEntity
                .ok()
                .body(
                        pacienteService.findAllPacientes())
                ;
    }

    @PreAuthorize("hasAuthority('get_paciente')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<List<Paciente>> findAll(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok()
                .body(
                        pacienteService.findPacienteById(id))
                ;
    }

    @PreAuthorize("hasAuthority('post_paciente')")
    @PostMapping("")
    public ResponseEntity<PacienteDto> save(@RequestBody @Validated PacienteDto dto) {
        PacienteMapper pacienteMapper = new PacienteMapper();
        return ResponseEntity.ok(pacienteService.save(dto));
    }

    @PreAuthorize("hasAuthority('put_paciente')")
    @PutMapping("/{id}")
    public ResponseEntity<PacienteEnteroDto> editar(@PathVariable(name = "id") int id,
                                                    @RequestBody PacienteEnteroDto dto) {
        return ResponseEntity.ok().body(pacienteService.editar(id,dto));
    }

    @PreAuthorize("hasAuthority('delete_paciente')")
    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "id") int id) {
        return ResponseEntity.ok(pacienteService.eliminar(id));
    }
}
