package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medico")
public class MedicoController {

    private final MedicoService medicoService;


    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    @PreAuthorize("hasAuthority('get_medico')")
    @GetMapping(value = "")
    public ResponseEntity<List<MedicoDto>> findAll(){
        return ResponseEntity
                .ok()
                .body(
                        medicoService.findAllMedico())
                ;

    }

    @PreAuthorize("hasAuthority('get_medico')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<List<MedicoDto>> findALl(@PathVariable(name = "id") Integer Id) {
        return ResponseEntity
                .ok()
                .body(
                        medicoService.findMedicoById(Id))
                ;
    }


    @PreAuthorize("hasAuthority('post_medico')")
    @PostMapping("")
    public ResponseEntity<MedicoDto> save(@RequestBody @Validated MedicoDto dto) {
        return ResponseEntity.ok(medicoService.save(dto));
    }

    @PreAuthorize("hasAuthority('put_medico')")
    @PutMapping("/{id}")
    public ResponseEntity<MedicoEnteroDto> editar(@PathVariable(name = "id") int id,
                                                  @RequestBody MedicoEnteroDto dto) {
        return ResponseEntity.ok().body(medicoService.editar(id,dto));
    }

   

    @PreAuthorize("hasAuthority('delete_medico')")
    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "id") int id) {
        return ResponseEntity.ok(medicoService.eliminar(id));
    }






}


