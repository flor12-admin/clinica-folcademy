package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;
import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.stereotype.Component;
import java.util.Optional;

@Component
public class TurnoMapper {
    public TurnoDto entityToDto(Turno entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoDto(
                                ent.getIdturno(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                ent.getIdmedico(),
                                ent.getIdpaciente()
                        )
                )
                .orElse(new TurnoDto());
    }

    public Turno dtoToEntity(TurnoDto dto) {
        Turno entity = new Turno();
        entity.setIdturno(dto.getIdturno());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setAtendido(dto.getAtendido());
        entity.setIdmedico(dto.getIdmedico());
        entity.setIdpaciente(dto.getIdpaciente());
        return entity;
    }

    public Turno enteroDtoToEntity(TurnoEnteroDto dto) {
        Turno entity = new Turno();
        entity.setIdturno(dto.getIdturno());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setAtendido(dto.getAtendido());
        entity.setIdmedico(dto.getIdmedico());
        entity.setIdpaciente(dto.getIdpaciente());
        return entity;
    }

    public TurnoEnteroDto entityToEnteroDto(Turno entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoEnteroDto(
                                ent.getIdturno(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                ent.getIdmedico(),
                                ent.getIdpaciente()
                        )
                )
                .orElse(new TurnoEnteroDto());
    }
}