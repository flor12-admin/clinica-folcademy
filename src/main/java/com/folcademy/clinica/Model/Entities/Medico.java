package com.folcademy.clinica.Model.Entities;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "medico")
@Getter
@Setter
@ToString
@RequiredArgsConstructor


public class Medico extends MedicoDto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmedico", columnDefinition = "INT(10) UNSIGNED")
    Integer idmedico;
    @Column(name = "Nombre", columnDefinition = "VARCHAR")
    String nombre = "";
    @Column(name = "Apellido", columnDefinition = "VARCHAR")
    String apellido = "";
    @Column(name = "Profesion", columnDefinition = "VARCHAR")
    String profesion = "";
    @Column(name = "Consulta", columnDefinition = "VARCHAR")
    Integer consulta;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Medico medico = (Medico) o;
        return idmedico != null && Objects.equals(idmedico, medico.idmedico);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
