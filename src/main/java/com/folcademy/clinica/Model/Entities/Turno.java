package com.folcademy.clinica.Model.Entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "turno")
@Getter
@Setter
@ToString
@RequiredArgsConstructor

public class Turno {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idturno", columnDefinition = "INT(10) UNSIGNED")
    Integer idturno;
    @Column(name = "Fecha", columnDefinition = "DATE")
    LocalDate fecha = null;
    @Column(name = "Hora", columnDefinition = "TIME")
    LocalTime hora = null;
    @Column(name = "Atendido", columnDefinition = "TINYINT")
    Boolean atendido;
    @Column(name = "Idmedico", columnDefinition = "INT")
    Integer idmedico;
    @Column(name = "Idpaciente", columnDefinition = "INT")
    Integer idpaciente;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idpaciente",referencedColumnName = "idpaciente", insertable = false, updatable = false)
    private  Paciente paciente;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idmedico",referencedColumnName = "idmedico", insertable = false, updatable = false)
    private  Medico medico;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Turno turno = (Turno) o;
        return idturno != null && Objects.equals(idturno, turno.idturno);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
