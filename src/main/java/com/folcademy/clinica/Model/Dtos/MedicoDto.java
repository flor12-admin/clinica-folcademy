package com.folcademy.clinica.Model.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.GeneratedValue;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class MedicoDto {

    Integer idmedico;
    @NonNull
    String nombre;
    @NonNull
    String apellido;
    @NonNull
    String profesion;
    @NonNull
    Integer consulta;


}
