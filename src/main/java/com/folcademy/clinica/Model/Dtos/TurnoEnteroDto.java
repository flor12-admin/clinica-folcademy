package com.folcademy.clinica.Model.Dtos;


import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Turno;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;


import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class TurnoEnteroDto {
    Integer idturno;
    LocalDate fecha;
    LocalTime hora;
    Boolean atendido;
    Integer idmedico;
    Integer idpaciente;
}